# hbo-ict-client-proefpracticum-2019-2020

# Opdracht 1 Denkstappen
1. Kijk wat er al aan code gegeven is
1. Config.js toevoegen van globs, zodat de juiste bestanden worden geselecteerd
1. Task bestanden aanpassen (n.b. deze moeten een function returnen)
   1. Kijk in de index welke parameters de taken meekrijgen in de index.js
   1. Voeg deze toe aan de functies in de tasks
   1. Controleer of de juiste imports beschikbaar zijn per task
   1. Implementeer de tasks en laat ze een function returnen.
1. Run 'gulp build' en valideer of er een dist map is aangemaakt :)

# Opdracht 2 Denkstappen
1. Kijk wat er al aan code gegeven is
1. Mario animatie
   1. voeg in style.css tussenpunten toe aan @keyframes mario
   1. koppel de keyframes met een animatie toe aan de selector van img#mario (https://www.w3schools.com/cssref/css3_pr_animation.asp)
   1. Run gulp en valideer je uitwerking
1. Responsiveness
   1. Voeg onderaan style.css een een mediaquery toe (https://www.w3schools.com/cssref/css3_pr_mediaquery.asp)
   1. binnen de het style blok van voeg je een nieuwe @keyframe toe (met dezelfde naam 'mario')
   1. de keyframe past alleen de top aan
   1. Run gulp en valideer je uitwerking
# Opdracht 3 Denkstappen
1. Kijk wat al aan code is gegeven
1. A+B Class constructor
   1. voeg een constructor toe met één property, cursor heeft een 'hard coded' waarde 0. (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/constructor)
   1. voeg een if controle toe een error gooit (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/throw)
1. C getNextMeme() toevoegen
   1. voeg een function toe getNextMeme() die een element uit de array teruggeeft.
   1. iedere keer dat deze functie wordt aangeroepen moet de cursos ook één worden opgehoogd.
   1. voeg voordat je het element teruggeeft een if controle uit zodat de cursor niet te groot wordt.
1. D searchMeme(search) functie toevoegen
   1. is er een functe in javascript die mij zou kunnen helpen?
   1. Google of implementeer zelf een loop
   1. implementeer de find functie i.c.m. includes (https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)
   ```
   [{id:1,name:'Hallo'},{id:12,name:'Hey!'}].filter(item => (item.name=='Hey!'));
   [{id:1,name:'Hallo'},{id:12,name:'Hey!'}].filter(item => (item.name.includes('He')));
   ```
1. E tests
   1. Kijk wat er al gegeven is aan code
   1. Zorg dat alle test met een 'expect(true).toBe(false);' werken, wel een fout teruggeven
   1. maak gebruik van 'fit' om de focussen op de test waaraan jij werkt
   1. ```expect(typeof f).toThrow('function')```
   1. ```expect(f).toThrow()```
   1. ```expect(typeof m.searchMeme).toBe('function')```
# Opdracht 4 Denkstappen
1. Kijk wat er al gegeven is aan code
1. A Module Pattern
   1. Gebruik het module pattern: https://gitlab.com/ekkebus/hbo-ict-client-01
1. D Event listner
   1. Click event: https://api.jquery.com/click/
   1. Gebruik https://api.jquery.com/html/ en https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
1. E Voeg toe aan memelist
   1. Geef ook de selector door van de UL
   1. Gebruik: https://api.jquery.com/append/ op LI toe te voegen
   1. _meme.getNextMeme().name
1. F Schrijf een clicktest
   1. Asynchroniteit! De test moet pas klikken als de init klaar is
   1. Selecteer het aantal LI van de #memes
  
