const {src, dest} = require('gulp');

const fn = function (filesJSON) {
    return function () {
        return src('/vendor/**/*.json')
            .pipe(dest('./dist'));
    }
};

exports.json = fn;
